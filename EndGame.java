import java.lang.reflect.Array;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Emma
 */
public class EndGame extends Application{
Text victoryStatus;
Text statistics;
public EndGame(int compPieces, int playerPieces){
    if (compPieces<playerPieces){
        victoryStatus = new Text ("You Lost");
        statistics = new Text("Your Score: "+ playerPieces +"\nComputer Score: "+ compPieces);
    } else if (compPieces == playerPieces){
        victoryStatus = new Text ("It was a tie!");
        statistics = new Text("Your Score: "+ playerPieces+"\nComputer Score: "+compPieces);
    }else if (compPieces > playerPieces){
        victoryStatus = new Text ("Congratulations!! You Won!");
        statistics = new Text("Your Score: "+ playerPieces+"\nComputer Score: "+compPieces);
       
    }
    
}
    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane pane = new BorderPane();
        pane.setPrefSize(400, 400);
        HBox buttonBox = new HBox();
        HBox status = new HBox();
        victoryStatus.setFont(Font.font(40));
        statistics.setFont(Font.font(30));
        Button playAgainButton = new Button("Play Again?");
        Button quitButton = new Button("Quit");
        playAgainButton.setOnAction(new AgainHandler());
        quitButton.setOnAction(new QuitHandler());
        buttonBox.getChildren().addAll(quitButton, playAgainButton);
        status.getChildren().add(victoryStatus);
        status.setPadding(new Insets(10,10,10,10));
        status.setAlignment(Pos.CENTER);
        buttonBox.setPadding(new Insets(10,10,10,10));
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(50);
        pane.setTop(status);
        pane.setCenter(statistics);
        pane.setBottom(buttonBox);
        Scene scene = new Scene(pane);
        primaryStage.setTitle("Blokus: Game Over");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
   
    public class QuitHandler implements EventHandler<ActionEvent> {

        /**
         * Handles "quit" button by closing the program.
         *
         * @param event
         */
        @Override
        public void handle(ActionEvent event) {
            Stage stage = new Stage();

            Node source = (Node) event.getSource();
            stage = (Stage) source.getScene().getWindow();
            stage.close();
        }

    }
    public class AgainHandler implements EventHandler<ActionEvent> {

        /**
         * Handles "again" button event by restarting the StartMenu and closing
         * this window.
         *
         * @param event
         */
        @Override
        public void handle(ActionEvent event) {
            StartMenu menu = new StartMenu();
            Stage stage = new Stage();
            menu.start(stage);
            Node source = (Node) event.getSource();
            stage = (Stage) source.getScene().getWindow();
            stage.close();
        }
    }

  
    
}

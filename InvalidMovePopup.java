
import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class creates a window that pops up when the player tries to make an 
 * invalid move. 
 * @author Emma
 */
public class InvalidMovePopup extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane pane = new BorderPane();
        pane.setPrefSize(300, 80);
        VBox box = new VBox();
        Text invalidText = new Text("INVALID MOVE");
        invalidText.setFont(Font.font(40));
        invalidText.setFill(Color.RED);
        box.setPadding(new Insets(10,10,10,10));
        pane.getChildren().add(invalidText);
        box.getChildren().add(invalidText);
        pane.getChildren().add(box);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();
        PauseTransition delay = new PauseTransition(Duration.seconds(1));
        delay.setOnFinished( event -> primaryStage.close() );
        delay.play();
    }
    
}

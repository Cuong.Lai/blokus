/*
 * PlacedSquare are the squares that makeup the gameboard
 * has fields for coordinates within board
 * Mouse events for placing pieces defined here
 */

/**
 *
 * @author Emma
 * edited by John
 */
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class PlacedSquare extends GridPane {
	int xCoord;
	int yCoord;
	int colorNum;
        int clearMoveCoordX;
        int clearMoveCoordY;
	GameBoard containingBoard;
	Rectangle aSquare = new Rectangle(19,19);
	TestB game = new TestB();
        InvalidMovePopup invalid = new InvalidMovePopup();
	
	public PlacedSquare(GameBoard passedBoard, int x, int y){
                
		xCoord = x;
		yCoord = y;
		containingBoard = passedBoard;
		aSquare.setFill(Color.WHITE);
                
		colorNum = 0;
		this.getChildren().add(aSquare);
		this.setPrefSize(20, 20);
		this.setStyle("-fx-border-color: black");
		
		
		/*
                * highlights piece on game board before player places the piece
                */
                this.setOnMouseEntered(e->{
                    Node source = (Node)e.getTarget() ;
                    Integer colIndex = this.getColumnIndex(source);
                    Integer rowIndex = this.getRowIndex(source);
                    containingBoard.showPiece(colIndex.intValue(),rowIndex.intValue());
                    containingBoard.setClearCoordX(colIndex.intValue());
                    containingBoard.setClearCoordY(rowIndex.intValue());
                });
                this.setOnMouseExited(e->{
                    Node source = (Node)e.getTarget() ;
                    Integer colIndex = this.getColumnIndex(source);
                    Integer rowIndex = this.getRowIndex(source);
                    containingBoard.clearShowPiece(colIndex.intValue(),rowIndex.intValue());
                    containingBoard.setClearCoordX(colIndex.intValue());
                    containingBoard.setClearCoordY(rowIndex.intValue());
                });
                /*
		 * When a placedSquare object is clicked, it calls the addPiece method in GameBoard and passes 
                 * the coordinates of this square
		 */
	        this.setOnMouseClicked(new EventHandler<MouseEvent>() {             
	            @Override
	            public void handle(MouseEvent event) {
	              
	                clearMoveCoordX=xCoord;
                        clearMoveCoordY=yCoord;
	                boolean success = containingBoard.addPiece(xCoord,yCoord);           
	                
                        if (!success){
                            Stage stage = new Stage();
                        try {
                            invalid.start(stage);
                        } catch (Exception ex) {
                            Logger.getLogger(TestB.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Node source = (Node) event.getSource();
                        stage = (Stage) source.getScene().getWindow();
                        }
                                                                                         
	            } 
	         });

	}
}










/**
 * This class creates each of the 21 pieces.The piece consists of an arraylist of Bsquares. This class also handles
 * the piece flip and rotate actions and removing the piece from the correct player's hands
 * @author Emma
 * edited by John
 */
import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.paint.Color;

public class Bpiece {

    ArrayList<Bsquare> allPieceSquares = new ArrayList<Bsquare>();

    Color correctColor = Color.WHITE;
    int position;
    Ahand containingHand;

    public boolean stillInHand = true;
    /*
    Construct a piece in the correct hand, of the right shape and in the 
    right hand
    */
    public Bpiece(int ID, int originX, int originY, Ahand theHand) {
        containingHand = theHand;
        switch (theHand.colorIdNum) {
            case 1:
                break;
            case 2:
                correctColor = Color.RED;
                break;
            case 3:
                correctColor = Color.LIMEGREEN;
                break;
            case 4:
                correctColor = Color.BLUE;
                break;
            case 5:
                correctColor = Color.YELLOW;
                break;
        }
        //Construct each of the 21 pieces for all hands
        switch (ID) {
            case 1:

                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                position = 0;
                break;
            case 2:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);

                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                position = 1;
                break;
            case 3:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY + 1]);
                position = 2;
                break;
            case 4:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                position = 3;
                break;
            case 5:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY + 1]);
                position = 4;
                break;
            case 6:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                position = 5;
                break;
            case 7:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 2][originY]);
                position = 6;
                break;
            case 8:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY - 1]);
                position = 7;
                break;
            case 9:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                position = 8;
                break;
            case 10:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 2]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 3]);
                position = 9;
                break;
            case 11:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY + 1]);
                position = 10;
                break;
            case 12:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY + 1]);
                position = 11;
                break;
            case 13:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                position = 12;
                break;
            case 14:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY - 1]);
                position = 13;
                break;
            case 15:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 2]);
                position = 14;
                break;
            case 16:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 2][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY + 1]);
                position = 15;
                break;
            case 17:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY + 1]);
                position = 16;
                break;
            case 18:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                position = 17;
                break;
            case 19:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 2][originY]);
                position = 18;
                break;
            case 20:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX - 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY + 1]);
                position = 19;
                break;
            case 21:
                allPieceSquares.add(theHand.fullGrid[originX][originY]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 1]);
                allPieceSquares.add(theHand.fullGrid[originX][originY - 2]);
                allPieceSquares.add(theHand.fullGrid[originX + 1][originY]);
                allPieceSquares.add(theHand.fullGrid[originX + 2][originY]);
                position = 20;
                break;

        }

        changePieceNum();
        fillSquares();

    }

    /*
        Set all the Bsquares to know what piece it is a part of
     */
    private void changePieceNum() {
        for (int i = 0; i < allPieceSquares.size(); i++) {
            allPieceSquares.get(i).setPieceNum(position);
        }
    }

    /*
        cycle through arraylist and set them all the correct color
     */
    public void fillSquares() {

        for (int i = 0; i < allPieceSquares.size(); i++) {
            allPieceSquares.get(i).aSquare.setFill(correctColor);
        }
    }

    /*
        Rotate the entire piece 90 degrees to the left by changing the relation of each
        square to the origin squre
     */
    public void rotateLeft() {

        int baseX = allPieceSquares.get(0).xCoord;
        int baseY = allPieceSquares.get(0).yCoord;

        for (int i = 0; i < allPieceSquares.size(); i++) {
            allPieceSquares.get(i).aSquare.setFill(Color.WHITE);
            allPieceSquares.get(i).pieceNum = 0;
        }

        for (int i = 0; i < allPieceSquares.size(); i++) {
            int realitiveX = allPieceSquares.get(i).xCoord - baseX;
            int realitiveY = allPieceSquares.get(i).yCoord - baseY;
            int newX = baseX + realitiveY;
            int newY = baseY - realitiveX;

            containingHand.fullGrid[newX][newY].pieceNum = position;
            containingHand.fullGrid[newX][newY].aSquare.setFill(correctColor);
            allPieceSquares.set(i, containingHand.fullGrid[newX][newY]);
        }

    }
    /*
    Rotates the entire piece 90 degrees to the right by moving every square
    relative to the origin square
    */
    public void rotateRight() {
        int baseX = allPieceSquares.get(0).xCoord;
        int baseY = allPieceSquares.get(0).yCoord;

        for (int i = 0; i < allPieceSquares.size(); i++) {
            allPieceSquares.get(i).aSquare.setFill(Color.WHITE);
            allPieceSquares.get(i).pieceNum = 0;
        }

        for (int i = 0; i < allPieceSquares.size(); i++) {
            int realitiveX = allPieceSquares.get(i).xCoord - baseX;
            int realitiveY = allPieceSquares.get(i).yCoord - baseY;
            int newX = baseX - realitiveY;
            int newY = baseY + realitiveX;

            containingHand.fullGrid[newX][newY].pieceNum = position;
            containingHand.fullGrid[newX][newY].aSquare.setFill(correctColor);
            allPieceSquares.set(i, containingHand.fullGrid[newX][newY]);
        }

    }
    /*
    Flips the entire piece by moving each square in the piece relative to the 
    origin square
    */
    public void flip() {
        int baseX = allPieceSquares.get(0).xCoord;
        int baseY = allPieceSquares.get(0).yCoord;
        

        for (int i = 0; i < allPieceSquares.size(); i++) {
            allPieceSquares.get(i).aSquare.setFill(Color.WHITE);
            allPieceSquares.get(i).pieceNum = 0;
        }

        for (int i = 0; i < allPieceSquares.size(); i++) {
            int realitiveX = allPieceSquares.get(i).xCoord - baseX;
            int realitiveY = allPieceSquares.get(i).yCoord - baseY;
            int newX = baseX + realitiveX;
            int newY = baseY - realitiveY;

            containingHand.fullGrid[newX][newY].pieceNum = position;
            containingHand.fullGrid[newX][newY].aSquare.setFill(correctColor);
            allPieceSquares.set(i, containingHand.fullGrid[newX][newY]);
        }
    }

    /*
        Get the size of the piece (e.g the number of squares)
     */
    public int getLengthOfPiece() {
        return allPieceSquares.size();
    }

    /*
        Gets the x coordinates of squares relative to the origin square
     */
    public ArrayList<Integer> getAllRealitiveXCoords() {
        ArrayList<Integer> temp = new ArrayList<Integer>();
        int baseX = allPieceSquares.get(0).xCoord;
        for (int i = 0; i < allPieceSquares.size(); i++) {
            temp.add(allPieceSquares.get(i).xCoord - baseX);
        }
        return temp;

    }

    /*
        Gets Y coordinates of all squares in a piece
     */
    public ArrayList<Integer> getAllRealitiveYCoords() {
        ArrayList<Integer> temp = new ArrayList<Integer>();
        int baseY = allPieceSquares.get(0).yCoord;
        for (int i = 0; i < allPieceSquares.size(); i++) {
            temp.add(allPieceSquares.get(i).yCoord - baseY);
        }
        return temp;

    }

    /*
        Remove method: sets the squares that were in the piece to be white and
	to contain the piece number 0
     */
    public void remove() {

        for (int i = 0; i < allPieceSquares.size(); i++) {
            allPieceSquares.get(i).setPieceNum(0);
            allPieceSquares.get(i).aSquare.setFill(Color.WHITE);
        }
        stillInHand = false;
    }

}

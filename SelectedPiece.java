/*
 *SelectedPiece tracks the piece selected by user, 
 *it uses a turn field to only allow a piece to be selected at the correct turn
 *Gameboard class uses this class to perform validity checks and place pieces on the board
 */

/**
 *
 * @author Emma
 * edited by John
 */
import java.util.ArrayList;

import javafx.scene.paint.Color;

public class SelectedPiece {
	Color correctColor = Color.BLACK;	                 
	public Ahand[] containingHandArray= new Ahand[4];    
	public int turn;                                            
	public int indexOfPiece;                                    
	public int allTurns;
	public SelectedPiece(Ahand Ahand1,Ahand Ahand2,Ahand Ahand3,Ahand Ahand4){                         
		
		containingHandArray[0] = Ahand1;                                     
		containingHandArray[1] = Ahand3;                                     
		containingHandArray[2] = Ahand2;
		containingHandArray[3] = Ahand4;
		turn = 0;                     
		indexOfPiece =0;
		allTurns = 0;
	}
	/*
        * Removes piece from player's hand once it is placed
        */
	public void removePiece(){
		
		indexOfPiece = containingHandArray[turn].selectedPiece;
		containingHandArray[turn].removePiece(indexOfPiece);
		allTurns++;
		turn = allTurns%4;
	}
        /*
        places piece on game board 
        */
	public void placePiece(){
		
		ArrayList<Integer> realiXs = containingHandArray[turn].getXCoordsOfSelected();
		ArrayList<Integer> realiYs = containingHandArray[turn].getYCoordsOfSelected();
		correctColor = containingHandArray[turn].handColor;
		indexOfPiece = containingHandArray[turn].selectedPiece;
		
	}
	/*
        gets the x coordinates of teh selected piece
        */
	public ArrayList<Integer> getRealiXCoords(){
		return containingHandArray[turn].getXCoordsOfSelected();
	}
	/*
        gets the y coordinates of the selected piece
        */
	public ArrayList<Integer> getRealiYCoords(){
		return containingHandArray[turn].getYCoordsOfSelected();
	}
        /*
        Returns the piece's color
        */
	public Color getColor(){
		return containingHandArray[turn].handColor;
				
	}
        /*
        returns the number indicator of the color of the hand
        */
	public int getColorNum() {
		return turn+1;
	}
        /*
        Returns whether or not a piece is still in the player's hand
        */
	public boolean stillInHand() {
		indexOfPiece = containingHandArray[turn].selectedPiece;
		return containingHandArray[turn].remainingPieces[indexOfPiece].stillInHand;
		
	}
        /*
        gets the current players turn identifier
        */
	public int getTurn(){
		return turn;
	}
	/*
        returns whether or not piece was set on the board
        */
	public boolean setPiece(int pieceNum){
		containingHandArray[turn].selectedPiece = pieceNum;
		if(containingHandArray[turn].remainingPieces[pieceNum].stillInHand){
			return true;
		}
		else{
			return false;
		}
	}
	/*
        changes the turns 
        */
        public  void changeTurns(){
           
            allTurns +=1;
            turn = allTurns%4;
            
        }
}





/**
 * This Class creates the hands of all players according to the correct color
 * and creates the pieces to display on the GUI in the correct location.
 *
 * @author Emma edited by John
 */
import java.util.ArrayList;

import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class Ahand {
    
    public Bpiece[] remainingPieces = new Bpiece[22];
    public GridPane playerHand;
    public Color handColor;
    int colorIdNum;
    int selectedPiece;
    Bsquare[][] fullGrid = new Bsquare[60][14];
    boolean noMoreMoves = false;

    public Ahand(int playerNum, GridPane pHand) {

        selectedPiece = 0;
        colorIdNum = playerNum;
        playerHand = pHand;
        int size = 16;
        if (playerNum == 2) {
            handColor = Color.RED;
        }
        if (playerNum == 3) {
            handColor = Color.LIMEGREEN;
        }
        if (playerNum == 4) {
            handColor = Color.BLUE;
        }
        if (playerNum == 5) {
            handColor = Color.YELLOW;
        }
        if (colorIdNum > 3) {
            size = 9;
        }
        //Loop creates a grid for this players hand
        for (int i = 0; i < 60; i++) {
            for (int j = 0; j < 14; j++) {
                fullGrid[i][j] = new Bsquare(size, 1, this, i, j);
                playerHand.add(fullGrid[i][j], i, j);
            }
        }

        //Create the pieces and add them to remainingPieces array THIS SHOULD BE TURNED IN TO A LOOP
        Bpiece firstTest = new Bpiece(1, 2, 2, this);
        remainingPieces[0] = firstTest;
        Bpiece secondTest = new Bpiece(2, 5, 2, this);
        remainingPieces[1] = secondTest;
        Bpiece Test3 = new Bpiece(3, 8, 2, this);
        remainingPieces[2] = Test3;
        Bpiece Test4 = new Bpiece(4, 13, 2, this);
        remainingPieces[3] = Test4;
        Bpiece Test5 = new Bpiece(5, 16, 2, this);
        remainingPieces[4] = Test5;
        Bpiece Test6 = new Bpiece(6, 20, 2, this);
        remainingPieces[5] = Test6;
        Bpiece Test7 = new Bpiece(7, 24, 2, this);
        remainingPieces[6] = Test7;
        Bpiece Test8 = new Bpiece(8, 30, 2, this);
        remainingPieces[7] = Test8;
        Bpiece Test9 = new Bpiece(9, 35, 2, this);
        remainingPieces[8] = Test9;
        Bpiece Test10 = new Bpiece(10, 52, 9, this);
        remainingPieces[9] = Test10;
        Bpiece Test11 = new Bpiece(11, 45, 2, this);
        remainingPieces[10] = Test11;
        Bpiece Test12 = new Bpiece(12, 50, 2, this);
        remainingPieces[11] = Test12;
        Bpiece Test13 = new Bpiece(13, 55, 2, this);
        remainingPieces[12] = Test13;
        Bpiece Test14 = new Bpiece(14, 4, 8, this);
        remainingPieces[13] = Test14;
        Bpiece Test15 = new Bpiece(15, 9, 8, this);
        remainingPieces[14] = Test15;
        Bpiece Test16 = new Bpiece(16, 14, 8, this);
        remainingPieces[15] = Test16;
        Bpiece Test17 = new Bpiece(17, 20, 8, this);
        remainingPieces[16] = Test17;
        Bpiece Test18 = new Bpiece(18, 26, 8, this);
        remainingPieces[17] = Test18;
        Bpiece Test19 = new Bpiece(19, 32, 8, this);
        remainingPieces[18] = Test19;
        Bpiece Test20 = new Bpiece(20, 38, 8, this);
        remainingPieces[19] = Test20;
        Bpiece Test21 = new Bpiece(21, 44, 8, this);
        remainingPieces[20] = Test21;

    }

    /*
        counts the remaining squares for each of the players to calculate the score
     */
    public int squaresRemaining() {
        int squaresLeft = 0;
        for (int i = 0; i < remainingPieces.length - 1; i++) {
            if (remainingPieces[i].stillInHand) {
                squaresLeft += remainingPieces[i].getLengthOfPiece();
            }
        }
        return squaresLeft;

    }

    /*
        Calls the remove method on the piece its self first
     */
    public void removePiece(int pieceNum) {

        remainingPieces[pieceNum].remove();
    }

    /*
	 * This method is called on the entire hand, but all it does is call the 
	 * real rotate method on the correct piece (the last one that was clicked)
     */
    public void rotateLeft() {

        if (remainingPieces[selectedPiece].stillInHand) {
            remainingPieces[selectedPiece].rotateLeft();
        }
    }

    /*
	 * This method is called on the entire hand, but all it does is call the 
	 * real rotate method on the correct piece (the last one that was clicked)
     */
    public void rotateRight() {
        if (remainingPieces[selectedPiece].stillInHand) {
            remainingPieces[selectedPiece].rotateRight();
        }
    }

    /*
        Calls the Bpiece flip method on the correct piece. 
    
     */
    public void Flip() {
        if (remainingPieces[selectedPiece].stillInHand) {
            // TODO Auto-generated method stub
            remainingPieces[selectedPiece].flip();
        }
    }

    /*
        This method sets one piece within this hand as being "selected" so that the rotate methods rotate the correct piece
     */
    public void setSelectedPiece(int selected) {
        if (remainingPieces[selectedPiece].stillInHand) {
            for (int i = 0; i < remainingPieces[selectedPiece].allPieceSquares.size(); i++) {
                remainingPieces[selectedPiece].allPieceSquares.get(i).aSquare.setFill(handColor);
            }
        }
        selectedPiece = selected;
        for (int i = 0; i < remainingPieces[selectedPiece].allPieceSquares.size(); i++) {
            remainingPieces[selectedPiece].allPieceSquares.get(i).aSquare.setFill(Color.ORANGE);
        }

    }

    /*
        Method returns all the relative x coordinates of selected piece as an arraylist
     */
    public ArrayList<Integer> getXCoordsOfSelected() {
        return remainingPieces[selectedPiece].getAllRealitiveXCoords();
    }

    /*
        Method returns all the relative y coordinates of selected piece as an arraylist
     */
    public ArrayList<Integer> getYCoordsOfSelected() {
        return remainingPieces[selectedPiece].getAllRealitiveYCoords();
    }

}

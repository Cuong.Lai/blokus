import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Displays the help information for the game. 
 * @author Emma
 */
public class Help extends Application{
    
    
    
    @Override
    public void start(Stage primaryStage) throws Exception {
     Text exA = new Text("Example A");
     Text exB = new Text("Example B");
      Label info;
     Label examples;
    
     ImageView valid = new ImageView("valid1.png");
     ImageView invalid = new ImageView("invalid.png");
     ImageView valid2 = new ImageView("valid2.png");
        info = new Label("To select a piece click on the desired piece in the appropriate color. "
                + "the color whose turn it is will be indicated on the right of the screen."
                + "to rotate the piece use the arrow keys. Left and right will rotate and up and down will flip"
               +"\nto place the piece on the board, move the mouse over the board. The piece you selected will"
        + "be outlined on the board. To commit the placement click on the board\n\n");
        examples = new Label("Invalid Move:\nExample of an invalid move. Trying to place a piece on top of a piece of another "
                        + "color.Place a piece of adjacent to a piece of the same color (SEE EXAMPLE A)"
                                + " Valid Move: Two pieces of the same color only touch at a corner (SEE EXAMPLE B)\n");
        BorderPane pane = new BorderPane();
        VBox infoBox = new VBox();
        HBox imageBox = new HBox();
        info.setWrapText(true);
        examples.setWrapText(true);
        imageBox.getChildren().addAll(valid,valid2);
        imageBox.setAlignment(Pos.CENTER);
        imageBox.setSpacing(20);
        pane.setPrefWidth(900);
        Button closeButton = new Button("Close");
        closeButton.setOnAction(new closeHandler());
        infoBox.getChildren().addAll(info,examples,exA,invalid,exB,imageBox,closeButton);
        infoBox.setPadding(new Insets(10,10,10,10));
        infoBox.setAlignment(Pos.CENTER);
        pane.setCenter(infoBox);
        Scene scene = new Scene(pane);
        primaryStage.setTitle("Blokus: Help Menu");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    /**
    * Closes the current window to return to the game
    */

    public class closeHandler implements EventHandler<ActionEvent>{
        
        @Override
        public void handle(ActionEvent event) {
            Stage stage = new Stage();

            Node source = (Node) event.getSource();
            stage = (Stage) source.getScene().getWindow();
            stage.close();
        }
        
    }
    
}




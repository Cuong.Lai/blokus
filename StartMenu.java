import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Emma
 */
public class StartMenu extends Application {

    public TestB display = new TestB();
    public StartMenu menu = this;
    public int chosenDifficulty = 0;

    /**
     * Sets up the primary buttons and text for this window. This is where the
     * difficulty level is set, and the directions are.
     *
     * @param primaryStage the stage which contains all nodes
     */
    @Override
    public void start(Stage primaryStage) {
        BorderPane pane = new BorderPane();
        RadioButton easy = new RadioButton("easy");
        easy.setOnAction(new difficultyHandler("easy"));
        RadioButton medium = new RadioButton("medium");
        medium.setOnAction(new difficultyHandler("medium"));
        RadioButton hard = new RadioButton("hard");
        hard.setOnAction(new difficultyHandler("hard"));
        Button play = new Button("Play");
        play.setOnAction(new playHandler());
        play.setStyle("-fx-background-color: lightgreen;");
        HBox buttonPane = new HBox();
        buttonPane.setSpacing(40);
        buttonPane.setAlignment(Pos.CENTER);
        pane.setPadding(new Insets(10, 10, 10, 10));
        buttonPane.getChildren().addAll(easy, medium, hard, play);
        buttonPane.setPadding(new Insets(20,20,20,20));
        
        String text = "To win Blokus game : \n" +
"The player's goal in this game is to place all or maximum of their 21 pieces on the board.\n" +
"\n" +
"Rules of Blokus Classic :\n" +
"\n" +
"Each player gets to place one piece per turn. The goal of the game is to play all your pieces "
                + "eventually. Each player should begin in a separate corner and works inward from there. "
                + "After the first turn, you can play anywhere on the board.\n" +
"\n" +
"The most important and ONLY rule of Blokus - the piece you play must touch the corner of at least one "
                + "of your other pieces and should never touch the side of any of your own pieces. "
                + "You can touch the sides of other players' pieces.\n" +
"\n" +
"\n" +
"End of the game : \n" +
"The game ends for a player when a player is blocked and is not able to place any pieces on the board according to the rules. "
                + "The game itself ends when no one can place any more pieces on the board anymore.\n" +
"\n" +
"Scoring :\n" +
"\n" +
"After the game ends, count the number of pieces each player unable to place on the board. Each square that "
                + "is not placed counts as a negative point.Award a bonus of 15 points if all 21 pieces got "
                + "placed on the board. \n" +
"A bumper bonus of to 20 points is awarded if the 21 pieces were placed on the board and the last piece placed "
                + "is a single square. The player with maximum points wins.\n";
        
        Label instructions = new Label(text);
        
        VBox instructionPane = new VBox();
        instructions.setWrapText(true);
        instructionPane.setPrefSize(500, 500);
        instructionPane.getChildren().add(instructions);
        instructions.setTextAlignment(TextAlignment.CENTER);
        instructionPane.setAlignment(Pos.CENTER);
        Text header = new Text("Instructions");
        header.setFont(new Font(20));
        HBox title = new HBox();
        title.getChildren().add(header);
        title.setAlignment(Pos.CENTER);
        pane.setCenter(instructionPane);
        pane.setBottom(buttonPane);
        pane.setTop(title);
        Scene scene = new Scene(pane);
        primaryStage.setTitle("Blokus: Start Menu");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Handles the toggle button for difficulty level. Sets the grid size for
     * the game.
     */
    class difficultyHandler implements EventHandler<ActionEvent> {

        String difficulty;

        /**
         * Handles the difficulty toggle buttons.
         *
         * @param d the string that denotes the difficulty
         */
        private difficultyHandler(String d) {
            difficulty = d;
        }

        /**
         * Main constructor for the difficulty toggle buttons
         *
         * @param event the button press.
         */
        @Override
        public void handle(ActionEvent event) {
            if (difficulty.equals("easy")) {

                chosenDifficulty = 1;
            } else if (difficulty.equals("medium")) {

                chosenDifficulty = 2;
            } else {

                chosenDifficulty = 3;
            }
        }

    }

    /**
     * Starts the Blokus window and closes this window
     */
    class playHandler implements EventHandler<ActionEvent> {

        /**
         * handles the event of the play button. Starts a new Set game and
         * closes this window.
         *
         * @param event the button press.
         */
        @Override
        public void handle(ActionEvent event) {
            Stage stage = new Stage();
            display.start(stage);
            display.setDifficulty(chosenDifficulty);
            Node source = (Node) event.getSource();
            stage = (Stage) source.getScene().getWindow();
            stage.close();
        }

    }

    /**
     * Launches the program
     *
     * @param args all pieces of the program
     */
    public static void main(String[] args) {
        launch(args);
    }

}



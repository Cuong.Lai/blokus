
/*
 * Bsquare are the squares that make up the pieces in the hand grids
 * they are gridpanes that have as fields thier coordinate within the hand, and their color
 */

/**
 *
 * @author Emma
 * edited by John
 */
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;


public class Bsquare extends GridPane {                   
	int size;                                             
	int pieceNum;                                         
	int xCoord;                                           
	int yCoord;
	int colorNum;                                         
	Ahand containingHand;                                
	
	Rectangle aSquare = new Rectangle(11, 11);           
	
	public Bsquare(int squareSize,int colorID,Ahand passedHand,int x,int y){     
																				 
		/*Initialize values
		 * (purpose of values outlined above)
		 */
		xCoord = x;
		yCoord =y;
		colorNum = colorID;
		containingHand = passedHand;
		aSquare.setHeight(squareSize-1);
		aSquare.setWidth(squareSize-1);
		size = squareSize;
		
		switch (colorID){                                                       
    	case 1:
		    aSquare.setFill(Color.WHITE);
		break;
    	case 2:
    	    aSquare.setFill(Color.RED);
    	break;
    	case 3:
        	aSquare.setFill(Color.GREEN);
        	break;
    	case 4:
        	aSquare.setFill(Color.BLUE);
        	break;
    	case 5:
        	aSquare.setFill(Color.YELLOW);
        	break;
		}	
		
		this.getChildren().add(aSquare);                                   
		
		this.setPrefSize(size, size);                                        
                
	/*
	 * When a Bsquare object is clicked, it knows what Ahand it is in
	 * and it knows what piece within that hand it is in. 
	 * 
	 * When the square is clicked it updates the selectedPiece field in the Ahand object 
	 * so that we know what piece was clicked
	 */
        
        this.setOnMouseClicked(new EventHandler<MouseEvent>() {              
            @Override
            public void handle(MouseEvent t) {
                        
                  	containingHand.setSelectedPiece(pieceNum);             
                	
                	
                
            }
           
         });
     
        }
	
	/**
         * sets piece number identifier
         * @param newNum number identifier 
         */
	public void setPieceNum(int newNum){
		pieceNum = newNum;
	}
        
	/*
	 * Fill in Method changes the color of the square shape within the Bsquare
	 * based on the colorNum int field 
	 */
	public void fillIn(){
		if(pieceNum >0){
			switch (colorNum){                                       
	    	case 1:
			    aSquare.setFill(Color.WHITE);
			break;
	    	case 2:
	    	    aSquare.setFill(Color.RED);
	    	break;
	    	case 3:
	        	aSquare.setFill(Color.GREEN);
	        	break;
	    	case 4:
	        	aSquare.setFill(Color.BLUE);
	        	break;
	    	case 5:
	        	aSquare.setFill(Color.YELLOW);
	        	break;
			}	
		}
	}
	
	/**
         * gets X coordinates
         * @return 
         */
	public int getXcoord(){
		return xCoord;
	}
        /**
         * gets y coordinates 
         * @return 
         */
	public int getYcoord(){
		return yCoord;
	}
}



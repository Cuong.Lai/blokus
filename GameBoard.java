/*
 * GameBoard is a collection of PlacedSquares
 * Gameboard implements majority of game login
 * Move Validity us checked in this class
 * Computer moves are implemented in this class
 * Check end of game is implemented in this class
 */

/**
 *
 * @author Emma
 * edited by John
 */
import java.awt.BasicStroke;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class GameBoard {
	public GridPane theBoard;                               
	PlacedSquare[][] allSquares = new PlacedSquare[20][20]; 
	SelectedPiece pieceInPlay;                                  
	int clearCoordX;	
        int clearCoordY;
	int totalNumOfPieces = 4*21;
	int turnsWithNoMove = 0;
   int theDifficulty = 0;
	                                                                   
	public GameBoard(GridPane mainBoard,SelectedPiece inPlay){        
		pieceInPlay = inPlay;                                         
																	  
		theBoard = mainBoard;
		
		                                                            
		for(int i = 0;i<=19;i++){                                   
			for(int j = 0;j<=19;j++){                                
				allSquares[i][j] = new PlacedSquare(this,i,j);      
				theBoard.add(allSquares[i][j], i, j);
			}
		}
	
        }
	
	public void removePiece(){
		pieceInPlay.removePiece();
		 
	  
        }
        public void setClearCoordX(int x){
            clearCoordX=x;
        }
        public void setClearCoordY(int y){
            clearCoordY = y;
        }
	
	
        /**
         * addPiece Method
	 * is called from SelectedSquare object when the selected square is clicked
	 * it draws the most recently selected piece on the gameboard with the basesquare in the square that was clicked
	 * The 'turn' variable in selectedPiece object keeps track of what hand should be pulled from
	 * Then by knowing what hand is in play, the correct color is used, and the piece can be removed from array of remaining pieces in the hand
         * @param x
         * @param y
         * @return 
         */
   public boolean addPiece(int x,int y) {
	   boolean success = false;
	   if(pieceInPlay.stillInHand()){
		ArrayList<Integer> tempXs = pieceInPlay.getRealiXCoords();                  
		ArrayList<Integer> tempYs = pieceInPlay.getRealiYCoords();
		
		
		if(isMoveValid(x,y)){                                                       
		                                                                            
			
			for(int i = 0;i<tempXs.size();i++){                                     
			 allSquares[x+tempXs.get(i)][y+tempYs.get(i)].colorNum = pieceInPlay.getColorNum();     
			 allSquares[x+tempXs.get(i)][y+tempYs.get(i)].aSquare.setFill(pieceInPlay.getColor());  
			}
		removePiece();
		
		success = true;
		}
	   }
	   return success;
	}
   

    public void showPiece(int x, int y) {
        if (pieceInPlay.stillInHand() && (pieceInPlay.getColor()==Color.RED || pieceInPlay.getColor()==Color.LIMEGREEN)) {
            ArrayList<Integer> tempXs = pieceInPlay.getRealiXCoords();                  
            ArrayList<Integer> tempYs = pieceInPlay.getRealiYCoords();
            for (int i = 0; i < tempXs.size(); i++) {                                      
                if (x + tempXs.get(i) < 20 && x + tempXs.get(i) > -1
                        && y + tempYs.get(i) > -1 && y + tempYs.get(i) < 20) {
                    allSquares[x + tempXs.get(i)][y + tempYs.get(i)].aSquare.setStroke(pieceInPlay.getColor());   
                    allSquares[x + tempXs.get(i)][y + tempYs.get(i)].aSquare.setStrokeWidth(2);
                }
            }
        }
    }
  
    public void clearShowPiece(int x, int y) {
        if (pieceInPlay.stillInHand()) {
            ArrayList<Integer> tempXs = pieceInPlay.getRealiXCoords();                  
            ArrayList<Integer> tempYs = pieceInPlay.getRealiYCoords();
            for (int i = 0; i < tempXs.size(); i++) {
                if (x + tempXs.get(i) < 20 && x + tempXs.get(i) > -1
                        && y + tempYs.get(i) > -1 && y + tempYs.get(i) < 20) {
                    allSquares[x + tempXs.get(i)][y + tempYs.get(i)].aSquare.setStroke(null);   
                }
    }
      }
  }
   /*
    * isMoveValid returns true if move is valid, false if it is not
    * 
    * It currently checks if proposed move would place part of the piece outside of gameboard
    * or if proposed move would overlap with a piece already on the board
    * this should be updated to check other rules and probably broken into smaller methods
    * We also will need to implement undo last move somewhere around here. 
    */
   public Boolean isMoveValid(int x,int y){
	   boolean goodMove = true;
	   ArrayList<Integer> tempXs = pieceInPlay.getRealiXCoords();
	   ArrayList<Integer> tempYs = pieceInPlay.getRealiYCoords();	  
	   if(pieceInPlay.allTurns<4){                      
		if(!firstTurnCheck(x,y,tempXs,tempYs)){         
			return false;
		}
	   }else{
		   if(!diagCheck(x,y,tempXs,tempYs)){
			   return false;                            
		   }
	   }
	   if(!adjacentCheck(x,y,tempXs,tempYs)){           
		   return false;                               
	   }
	   
	   
	   for(int i = 0;i<tempXs.size();i++){
		                                                
		if(x+tempXs.get(i)>19||x+tempXs.get(i)<0||
					y+tempYs.get(i)<0||y+tempYs.get(i)>19){
				goodMove = false;
			}
		
		else if(allSquares[x+tempXs.get(i)][y+tempYs.get(i)].colorNum!=0){     
			goodMove = false;
		}
	   }
	   return goodMove;
   }
  
   /*
    * adjacent checks makes sure not adjacent squares of this move would be the same color
    */
   public Boolean adjacentCheck(int x,int y,ArrayList<Integer> Xlist,ArrayList<Integer> Ylist){
	   for(int i = 0;i<Xlist.size();i++){
		   int squareXC = x+Xlist.get(i);   
		   int squareYC = y+Ylist.get(i);
		   if((squareXC<=19&&squareXC>=0)&&(squareYC<=19&&squareYC>=0)){
		        if(((squareXC-1)>=0)&&(squareYC)>=0){                   
		             if(allSquares[squareXC-1][squareYC].colorNum == pieceInPlay.getColorNum()){  
			         return false;
		              }
		         }
		  
		        if(((squareXC)>=0)&&(squareYC-1)>=0){                   				   
				     if(allSquares[squareXC][squareYC-1].colorNum == pieceInPlay.getColorNum()){
			          return false;
		              }
		         }
		   if(((squareXC+1)<=19)&&(squareYC)>=0){                       			   
				   if(allSquares[squareXC+1][squareYC].colorNum == pieceInPlay.getColorNum()){
				    return false;
		          }
		   }
		   
		   if(((squareXC)<=19)&&(squareYC+1)<=19){	
				if(allSquares[squareXC][squareYC+1].colorNum == pieceInPlay.getColorNum()){
			    return false;
			  }
		     }
		   }
		   }
		 return true; 
   }
   
   /*
    * diagCheck checks to see if one of the squares in the piece will be diagonal to the same color
    */
   public Boolean diagCheck(int x,int y,ArrayList<Integer> Xlist,ArrayList<Integer> Ylist){
	   for(int i = 0;i<Xlist.size();i++){     
		 
		   int squareXC = x+Xlist.get(i);   
		   int squareYC = y+Ylist.get(i);
		   if((squareXC<=19&&squareXC>=0)&&(squareYC<=19&&squareYC>=0)){
         if (theDifficulty == 3) {
            if(((squareXC+1)<=19)&&(squareYC-1)>=0){				   
				   if(allSquares[squareXC+1][squareYC-1].colorNum == pieceInPlay.getColorNum()){
				  return true;
		          }
		      }
            if(((squareXC-1)>=0)&&(squareYC+1)<=19){				   
   				  if(allSquares[squareXC-1][squareYC+1].colorNum == pieceInPlay.getColorNum()){					 
   			      return true;
   		        }
   		   }
            if(((squareXC-1)>=0)&&(squareYC-1)>=0){  
		        if(allSquares[squareXC-1][squareYC-1].colorNum == pieceInPlay.getColorNum()){
			     return true;
		          }
		     }
   		   
   		   
   		   if(((squareXC+1)<=19)&&(squareYC+1)<=19){		
   				if(allSquares[squareXC+1][squareYC+1].colorNum == pieceInPlay.getColorNum()){
   			    return true;
   			  }
   		   }
         }
         else {
         
   		   if(((squareXC-1)>=0)&&(squareYC-1)>=0){  
   		        if(allSquares[squareXC-1][squareYC-1].colorNum == pieceInPlay.getColorNum()){
   			     return true;
   		          }
   		     }
   		   if(((squareXC-1)>=0)&&(squareYC+1)<=19){				   
   				  if(allSquares[squareXC-1][squareYC+1].colorNum == pieceInPlay.getColorNum()){					 
   			      return true;
   		        }
   		   }
            if(((squareXC+1)<=19)&&(squareYC-1)>=0){				   
				   if(allSquares[squareXC+1][squareYC-1].colorNum == pieceInPlay.getColorNum()){
				  return true;
		          }
		      }
   		   if(((squareXC+1)<=19)&&(squareYC+1)<=19){		
   				if(allSquares[squareXC+1][squareYC+1].colorNum == pieceInPlay.getColorNum()){
   			    return true;
   			  }
   		   }	
         }
		 }
       }
		 return false; 
   }
   
   
   
   /*
    * firstMove check returns true if move occupies the correct corner square this method is called 
    * when we are in the first 4 moves of the game
    */
   public Boolean firstTurnCheck(int x,int y,ArrayList<Integer> Xlist,ArrayList<Integer> Ylist){ 
	   int xStart =99;
	   int yStart = 99;
	   switch (pieceInPlay.allTurns){                      
	   case 0:
		   xStart = 19;
		   yStart = 19;
		   break;
	   case 1:
		   xStart = 0;
		   yStart = 19;
		   break;
	   case 2:
		   xStart = 0;
		   yStart = 0;
		   break;
	   case 3:
		   xStart = 19;
		   yStart = 0;
		   break;
	   default:
		   break;
	   }
	   for(int i = 0;i<Xlist.size();i++){
		   if(x+Xlist.get(i)==xStart&&y+Ylist.get(i)==yStart){
			   return true;
		   }
	   }
	    return false;
   }
   public boolean computerTurn(int difficulty, Ahand[] hands){
       
	  boolean somethingRemaining = false; 
     theDifficulty = difficulty;
	   for(int j = 0;j<21;j++){
		   pieceInPlay.setPiece(j);
			 if(pieceInPlay.stillInHand()){
				 somethingRemaining = true;
			 }
		 } 
	   if(!somethingRemaining){
		   pieceInPlay.containingHandArray[pieceInPlay.turn].noMoreMoves=true;
		   return false;
		   
	   }
	  Random someNumber = new Random();
      int largePieces = 9;
	  
      if (theDifficulty <= 1) {
    	  int attemptCounter=0;
    	  int cycleThrough=0;
	      int randomPieceNum = Math.abs((someNumber.nextInt() % 20)+1);
	      pieceInPlay.setPiece(randomPieceNum);
	      if(attemptCounter > 100){
	    	  for(int j = 0;j<21;j++){
	    		  pieceInPlay.setPiece(cycleThrough);
	    		  cycleThrough++;
	    		  if(cycleThrough ==20){
	    			return false;
	    		  }
	    	  }
	    		 }
	      while(!pieceInPlay.stillInHand() && attemptCounter < 100){
		      attemptCounter++;
	    	  randomPieceNum = Math.abs((someNumber.nextInt() % 20)+1);
		      pieceInPlay.setPiece(randomPieceNum);
		     
	      }
      }
      else if (theDifficulty == 2) {
         int randomPieceNum = Math.abs(((someNumber.nextInt(20 - largePieces)+largePieces) % 20)+1);
		   pieceInPlay.setPiece(randomPieceNum);
         if (pieceInPlay.stillInHand()) {
            largePieces--;
         }
	      while(!pieceInPlay.stillInHand()){
            randomPieceNum = Math.abs((someNumber.nextInt() % 20)+1);
		      pieceInPlay.setPiece(randomPieceNum);
         }
      }
      else {
         int randomPieceNum = Math.abs(((someNumber.nextInt(20 - largePieces)+largePieces) % 20)+1);
		   pieceInPlay.setPiece(randomPieceNum);
         if (pieceInPlay.stillInHand()) {
            largePieces--;
         }
	      while(!pieceInPlay.stillInHand()){
            randomPieceNum = Math.abs((someNumber.nextInt() % 20)+1);
		      pieceInPlay.setPiece(randomPieceNum);
         }
      }
	   int cX = 0;
	   int cY = 0;
           boolean piecePlaced = false;
	   for(int i = 0;i<=19;i++){
		   for(int j = 0;j<=19;j++){
                       for (int k = 0; k<8; k++){
                           
			   if(isMoveValid(i,j)){
				   addPiece(i,j);
				   i= 100;
				   j = 100;
                                  piecePlaced = true;
                           
                           }
                           int turn = pieceInPlay.getTurn();
                           if (k==3){
                               hands[turn].Flip();
                           }else{
                               hands[turn].rotateLeft();
                           }
                                   
			   }
		   }
	   }
           if (piecePlaced == false){
               pieceInPlay.changeTurns();
           }
           
               return true;
                      
	
           }
   public boolean checkEndGame(Ahand[] hands){
	 int thisTurnsTotalpieces = 0;
	 for(int i = 0;i<4;i++){
		 int remainingPiecesCounter = 0;
		 for(int j = 0;j<21;j++){
			 if(hands[i].remainingPieces[j].stillInHand){
				 
				 remainingPiecesCounter++;
			 }
			 thisTurnsTotalpieces += remainingPiecesCounter;
		 }
		if(thisTurnsTotalpieces ==totalNumOfPieces){
			turnsWithNoMove++;
		}
		else{
			totalNumOfPieces = thisTurnsTotalpieces;
		}
		 if (turnsWithNoMove >3){
			 return true;
		 }
	 }
	return false;
	   
   }

}



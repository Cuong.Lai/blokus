/*
 * This Class is our main method, creates all objects and GUI and begins game
 */

/**
 *
 * @author Emma
 * edited by John
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import static javafx.collections.FXCollections.fill;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


public class TestB extends Application {
    public Help help = new Help();
    public EndGame end;
    private int whichTurn = 0;
    SelectedPiece pieceInPlay;
    GameBoard centerBoard;
    int difficulty;
    int userSquaresRemain = 178;
    int computerSquaresRemain= 178;
    Ahand[] allHands;
    Text scoreText;
    Text turn = new Text ("It is Red's turn");
   public TestB(){
       
   }
	@Override 
	public void start(Stage primaryStage){
	difficulty =1;
	BorderPane main = new BorderPane();							
	GridPane testGrid = new GridPane();							
	GridPane testGrid2 = new GridPane();
	GridPane testGrid3 = new GridPane();
	GridPane testGrid4 = new GridPane();
	Button quitButton = new Button ("Quit");
        Button helpButton = new Button("Help");
        Button endTurnButton = new Button("End Turn");
        quitButton.setOnAction(new quitHandler());
        endTurnButton.setOnAction(new endTurnHandler());
        helpButton.setOnAction(new helpHandler());
        VBox hand1 = new VBox();
        VBox hand2 = new VBox();
        VBox buttonBox = new VBox();
        HBox boardBox = new HBox();
        VBox infoBox = new VBox();
	buttonBox.setFocusTraversable(false);
        quitButton.setFocusTraversable(false);
        helpButton.setFocusTraversable(false);
        endTurnButton.setFocusTraversable(false);
	Ahand testHand = new Ahand(2, testGrid);					
	Ahand testHand2 = new Ahand(3, testGrid2);
	Ahand testHand3 = new Ahand(4,testGrid3);
	Ahand testHand4 = new Ahand(5,testGrid4);
	Ahand[] aHand = {testHand,testHand3,testHand2,testHand4};   
        
        allHands = aHand;
	pieceInPlay = new SelectedPiece(testHand,testHand2,testHand3,testHand4);											
	GridPane gameBoardGrid = new GridPane();					
	centerBoard = new GameBoard(gameBoardGrid,pieceInPlay);
	
	
	 /**
     * Sets up key events for the arrow keys and space bar. All keys send 
     * messages to the game, which should react appropriately.
     */
   

        testGrid.setOnKeyPressed(e -> {
             testGrid.requestFocus();
            buttonBox.setFocusTraversable(false);
            centerBoard.clearShowPiece(centerBoard.clearCoordX, centerBoard.clearCoordY);
        	int theTurn = pieceInPlay.getTurn();
            switch (e.getCode()) {
                case LEFT:
                	theTurn = pieceInPlay.getTurn();
                        testGrid.requestFocus(); 
//                        centerBoard.clearShowPiece(centerBoard.clearCoordX, centerBoard.clearCoordY);
                	allHands[theTurn].rotateLeft();
                        
                        centerBoard.showPiece(centerBoard.clearCoordX, centerBoard.clearCoordY);
                    break;
                case RIGHT:
                	theTurn = pieceInPlay.getTurn();
                        testGrid.requestFocus();
                	allHands[theTurn].rotateRight();
                        centerBoard.showPiece(centerBoard.clearCoordX, centerBoard.clearCoordY);
                    break;
                case DOWN:
                	theTurn = pieceInPlay.getTurn();
                        testGrid.requestFocus();	
                	allHands[theTurn].Flip();
                        centerBoard.showPiece(centerBoard.clearCoordX, centerBoard.clearCoordY);
                        
                    break;
                case UP:
                        theTurn = pieceInPlay.getTurn();
                        testGrid.requestFocus();	
                	allHands[theTurn].Flip();
                        centerBoard.showPiece(centerBoard.clearCoordX, centerBoard.clearCoordY);
                        
                    break;
			default:
                            testGrid.requestFocus();
			break;

            }
        });
    scoreText = new Text("User squares left: "+userSquaresRemain+"\n\n\n"+
      			"Computer squares left: "+computerSquaresRemain);
    scoreText.setFont(Font.font(15)); 
    turn.setFill(Color.RED);
    turn.setFont(Font.font(20));
    buttonBox.setSpacing(30);
    infoBox.getChildren().addAll(scoreText,turn);
    infoBox.setSpacing(80);
    infoBox.setPadding(new Insets (40,20,0,20));
    buttonBox.setAlignment(Pos.CENTER);
    boardBox.setAlignment(Pos.CENTER);
    boardBox.setSpacing(10);
    buttonBox.setFocusTraversable(false);
    main.setCenter(boardBox);
    hand1.getChildren().add(testGrid);
    hand2.getChildren().add(testGrid2);
    testGrid.setAlignment(Pos.CENTER);
    testGrid2.setAlignment(Pos.CENTER);
    hand1.setAlignment(Pos.CENTER);
    hand2.setAlignment(Pos.CENTER);
    boardBox.getChildren().addAll(buttonBox,gameBoardGrid,infoBox);
    buttonBox.setPadding(new Insets (0,80,0,80));
    buttonBox.getChildren().addAll(endTurnButton,helpButton,quitButton);
    main.setBottom(hand1);
    main.setTop(hand2);
    testGrid.setPadding(new Insets (10, 10, 10, 10));
    testGrid2.setPadding(new Insets (10, 10, 10, 10));	
    Scene myScene = new Scene(main);
    primaryStage.setScene(myScene);
    primaryStage.sizeToScene();
    primaryStage.show();
    testGrid.requestFocus(); 
	
   
   
}
/*
        gets the pieces remaining in each hand
        */        
public int getPiecesRemain(Ahand hand1,Ahand hand3){
	int piecesRemain = 0;
	piecesRemain += hand1.squaresRemaining();
	piecesRemain += hand3.squaresRemaining();
	return piecesRemain;
}

/*
sets the difficulty based on the player's selection in the start menu
*/
public void setDifficulty(int chosenDifficulty) {
      difficulty = chosenDifficulty;
      System.out.print("The difficulty is ");
      System.out.println(difficulty);
   }
/*
Closes the game
*/
public class quitHandler implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event) {
            Stage stage = new Stage();
            Node source = (Node) event.getSource();
            stage = (Stage) source.getScene().getWindow();
            stage.close();
        }
    
}
/*
opens the help window
*/
public class helpHandler implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event) {
            Stage stage = new Stage();
            try {
                help.start(stage);
            } catch (Exception ex) {
                Logger.getLogger(TestB.class.getName()).log(Level.SEVERE, null, ex);
            }
            Node source = (Node) event.getSource();
            stage = (Stage) source.getScene().getWindow();
        }
}

/**
 * ends the player's turn and passes the turn to the computer. Also calculates score
 * 
*/
public class endTurnHandler implements EventHandler<ActionEvent>{

            @Override
            public void handle(ActionEvent event) {
            	userSquaresRemain = getPiecesRemain(allHands[0],allHands[2]);
            	scoreText.setText("User squares left: "+userSquaresRemain+"\n\n\n"+
            			"Computer squares left: "+computerSquaresRemain);
            	System.out.println("Number of userSquares remaining : " +userSquaresRemain);
                if (pieceInPlay.getTurn()==1||pieceInPlay.getTurn()==3){
                  if (pieceInPlay.getTurn()==1){
                      turn.setText("It's Green's turn");
                      turn.setFill(Color.GREEN);
                  }else if (pieceInPlay.getTurn()==3){
                      turn.setText("It's Red's turn");
                      turn.setFill(Color.RED);
                  }
                  
                  centerBoard.computerTurn(difficulty, allHands);
                  computerSquaresRemain = getPiecesRemain(allHands[1],allHands[3]);
                  
                  scoreText.setText("User squares left: "+userSquaresRemain+"\n\n\n"+
              			"Computer squares left: "+computerSquaresRemain);
                }
                else{
                    pieceInPlay.changeTurns();
                    if (pieceInPlay.getTurn()==1||pieceInPlay.getTurn()==3){
               
                    }                  
                }
                if(centerBoard.checkEndGame(allHands)){
                    end = new EndGame(computerSquaresRemain,userSquaresRemain);
                    Stage stage = new Stage();
                        try {
                            end.start(stage);
                        } catch (Exception ex) {
                            Logger.getLogger(TestB.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Node source = (Node) event.getSource();
                        stage = (Stage) source.getScene().getWindow();
                        stage.close();
                };
                
                
            }
    
}

}
